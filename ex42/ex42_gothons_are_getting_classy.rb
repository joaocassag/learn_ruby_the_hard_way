class Game

 def initialize(start)
  @quips = ["You died. Youd kinda suck at this.",
          "Nice Job, you died .. jackass.",
          "Such a looser.",
          "I have a small puppy that's better at this."
   ]
   @start = start
   @rooms = old_room
   puts "in init @start = " + @start.inspect
   #puts quips[rand(quips.length)]
   #Process.exit(1)
 end


 def prompt()
  print '> '
 end

 def play(map,start)
  puts "@start => "+ @start.inspect
  next_room = @start
  
  while true
   room = map[next_room]
   puts "\n----------"
   #room = method next_room
   next_room = room.call()
  end
 end

 def death()
  puts @quips[rand(@quips.length)]
  Process.exit(1)
 end

 def central_corridor()
  puts "The Gothons of Planet Percal #25 have invaded your ship and destroyed"
  puts "your entire crew. You are the last surviving member and your last"
  puts "mission is to get the nreutron destruct bomb from the Weapons Armory,"
  puts "put it in the bridge, and blow the ship up after getting into an "
  puts "escape pod."
  puts "\n"
  puts "You're running down the central corrdidor to the Weapons Armory when"
  puts "a Gothon jumps out, red scaly skin, dar grimy teeth, and evil clown costume"
  puts "flowing around his hate filled body. He's blocking the door to the"
  puts "Armory and about to pull a weapon to blast you."

  prompt
  action = gets.chomp

  if action == "shoot!"
   puts "Quick on the draw you yank out your blaster and fire it at the Gothon."
   puts "His clown costume  is flowing and moving around his body, which throws"
   puts "off your aim. Your laser hist his costume but misses him entirely. This"
   puts "completely ruins his brand new costume his mother bougth him, which"
   puts "make him fly into an insane rage and blast you repeatedly in the face until"
   puts "are dead. Then he eats you."
   return :death

  elsif action == "dodge!"
   puts "Like a world class boxer you dodge, weave, slip and slide right"
   puts "as a Gothon's blaster cranks a laser past your head."
   puts "In the middle of your artful dofge yout foot slips and you"
   puts "bang your head on the metal wall and pass out."
   puts "You wake up shortly after only to die as the Gothon stomps on"
   puts "your head and eats you."
   return :death
  elsif action == "tell a joke"
   puts "Lucky for you they made you learn Gothon insults in the academy."
   puts "You tell the one Gothon joke you know:"
   puts "Lbhe qljh kdkjd dkkf fffjhw uoruor pweoer jhklfl zmbkf ieoprs dhhdd ssdh ddjege fklff."
   puts "The Gothon stops, tries not to laugh, then busts out laughing an can't move."
   puts "While he's laughing you run up and shoot him square in the head"
   puts "putting him down, then jump through the Weapon Armory door."
   return :laser_weapon_armory
  else
   puts "DOES NOT COMPUTE!"
   return :central_corridor
  end
 end

 def laser_weapon_armory()
  puts "You do ...."
  puts "..."
  code = "%s%s%s" % [rand(9)+1,rand(9)+1,rand(9)+1]
  print " [keypad]> "
  guess = gets.chomp
  guesses = 0

  while guess != code and guesses < 10
   puts "BZZZZZEDDDD!"
   guesses +=1
   print "[keypad]> "
   guess = gets.chomp
  end

  if guess == code
   puts "The container clicks open ...."
   puts "...."
   puts "..."
   return :the_bridge
  else
   puts "The lock buszes one last time and then you hear ..."
   puts "..."
   puts "..."
   return :death
   end
 end

 def the_bridge()
  puts "You burst onto the Bridge with the netron ..."
  puts "..."
  puts "..."

  prompt
  action = gets.chomp

  if action == "throw the bomb"
   puts "In a panic you throw the ..."
   puts "..."
   return :death
  elsif action == "slowly place the bomb"
   puts "You point your blaster at the bomb ..."
   puts "..."
   puts "..."
   return :escape_pod
  else
   puts "DOES NOT COMPUTE!"
   return :the_bridge
  end
 end

 def escape_pod()
  puts "Your rush ..."
  puts "..."
  good_pod = rand(5)+1
  print "[pod #]> "
  guess = gets.chomp

  if guess.to_i != good_pod
   puts "You jump into pod %s ..." % guess
   puts "..."
   return :death
  else
   puts "You jump into pod %s ...." % guess
   puts "..."
   Process.exit(0)
  end
 end

 def old_room()
 	@ROOMS = {
	 :death => method(:death),
	 :central_corridor => method(:central_corridor),
	 :laser_weapon_armory => method(:laser_weapon_armory),
	 :the_bridge => method(:the_bridge),
	 :escape_pod => method(:escape_pod)
	}
 end

 def runner(map,start)
  next_one = start

  while true
   room = map[next_one]
   puts "\n----------"
   next_one = room.call()
  end
 end

end # class end
#runner(ROOMS, :central_corridor)

a_game = Game.new(:central_corridor)
ROOMS = a_game.old_room
a_game.play(ROOMS,:central_corridor)

