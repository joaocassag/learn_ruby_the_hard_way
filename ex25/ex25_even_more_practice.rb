module Ex25
 def self.break_words(stuff)
  words = stuff.split(' ')
  words
 end

 def self.sort_words(words)
  words.sort
 end

 def self.print_first_word(words)
  word = words.shift()
  puts word
 end

 def self.print_last_word(words)
  word = words.pop
  puts word
 end

 def self.sort_sentence(sentence)
  words = break_words(sentence)
  sort_words words
 end

 def self.print_first_and_last(sentence)
  words = break_words sentence
  print_first_word words
  print_last_word words
 end

 def self.print_first_and_last_sorted(sentence)
  words = sort_sentence sentence
  print_first_word words
  print_last_word words
 end
end

#require 'ex25'
include Ex25
s = "All good things come to those who wait."
words = Ex25.break_words s
#puts "words: #{words}"
sorted_words =  Ex25.sort_words words
#puts "sorted_words: #{sorted_words}"

#first_word = Ex25.print_first_word words
#puts "First word: #{first_word}"
#Ex25.print_first_word words

#last_word = Ex25.print_last_word words
#puts "Last word: #{last_word}"
#Ex25.print_last_word words

sorted_sentence = Ex25.sort_sentence s
#puts "Sorted sentence: #{sorted_sentence}"

#firs_and_last = Ex25.print_first_and_last s
#puts "First and last: #{first_and_last}"
#Ex25.print_first_and_last s

#first_and_last_sorted = Ex25.print_first_and_last_sorted s
#puts "First and last sorted: #{first_and_last_sorted}"
#Ex25.print_first_and_last_sorted s

